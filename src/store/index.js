import Vue from 'vue';
import Vuex from 'vuex';
import app from './modules/app';
import errorLog from './modules/errorLog';
import permission from './modules/permission';
import farmer from './modules/farmer';
import forum from './modules/forum';
import intermediasi from './modules/intermediasi';
import article from './modules/article';
import tagsView from './modules/tagsView';
import user from './modules/user';
import activity from './modules/activity'
import getters from './getters';
import recommendation from './modules/recommendation'

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    app,
    errorLog,
    permission,
    tagsView,
    user,
    farmer,
    article,
    forum,
    intermediasi,
    activity,
    recommendation
  },
  getters
});
export default store;

import { forumService } from '../../services/forumService';

const initialState = {
  countPost: 0,
  countAccessedForum: 0,
  countSharedPost: 0,
  countComment: 0,
  forumTopic: null
};

export default {
  namespaced: false,
  state: initialState,
  actions: {
    countPost({ commit }, params) {
      commit('FORUM_POST_REQUEST');
      return forumService
        .countPost(params)
        .then(function(data) {
          commit('FORUM_POST_SUCCESS', data);
        })
        .catch(err => {
          commit('FORUM_POST_FAILURE');
          throw err;
        });
    },
    countAccessedForum({ commit }, params) {
      commit('ACCESSED_FORUM_REQUEST');
      return forumService
        .countAccessedForum(params)
        .then(function(data) {
          commit('ACCESSED_FORUM_SUCCESS', data);
        })
        .catch(err => {
          commit('ACCESSED_FORUM_FAILURE');
          throw err;
        });
    },
    countSharedPost({ commit }, params) {
      commit('SHARED_POST_REQUEST');
      return forumService
        .countSharedPost(params)
        .then(function(data) {
          commit('SHARED_POST_SUCCESS', data);
        })
        .catch(err => {
          commit('SHARED_POST_FAILURE');
          throw err;
        });
    },
    countComment({ commit }, params) {
      commit('COMMENT_REQUEST');
      return forumService
        .countComment(params)
        .then(function(data) {
          commit('COMMENT_SUCCESS', data);
        })
        .catch(err => {
          commit('COMMENT_FAILURE');
          throw err;
        });
    },
    countForumTopic({ commit }, params) {
      commit('TOPIC_REQUEST');
      return forumService
        .countForumTopic(params)
        .then(function(data) {
          commit('TOPIC_SUCCESS', data);
        })
        .catch(err => {
          commit('TOPIC_FAILURE');
          throw err;
        });
    }
  },
  mutations: {
    FORUM_POST_REQUEST(state, params) {
      state.countPost = null;
    },
    FORUM_POST_SUCCESS(state, data) {
      state.countPost = data.data.post_count;
    },
    FORUM_POST_FAILURE(state, params) {
      state.countPost = null;
    },
    ACCESSED_FORUM_REQUEST(state, params) {
      state.countAccessedForum = null;
    },
    ACCESSED_FORUM_SUCCESS(state, data) {
      state.countAccessedForum = data.data[0].count;
    },
    ACCESSED_FORUM_FAILURE(state, params) {
      state.countAccessedForum = null;
    },
    SHARED_POST_REQUEST(state, params) {
      state.countSharedPost = null;
    },
    SHARED_POST_SUCCESS(state, data) {
      state.countSharedPost = data.data.total_shared;
    },
    SHARED_POST_FAILURE(state, params) {
      state.countSharedPost = null;
    },
    COMMENT_REQUEST(state, params) {
      state.countComment = null;
    },
    COMMENT_SUCCESS(state, data) {
      state.countComment = data.data.comment_count;
    },
    COMMENT_FAILURE(state, params) {
      state.countComment = null;
    },
    TOPIC_REQUEST(state, params) {
      state.forumTopic = null;
    },
    TOPIC_SUCCESS(state, data) {
      state.forumTopic = data.data;
    },
    TOPIC_FAILURE(state, params) {
      state.forumTopic = null;
    }
  }
};

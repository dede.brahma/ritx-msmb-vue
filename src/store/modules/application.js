import { applicationService } from '../../services/applicationService';

const initialState = {
  totalUser: 0,
  userActivity: [],
  appActivity: 0
};

export default {
  namespaced: false,
  state: initialState,
  actions: {
    getTotalUser({ commit }, params) {
      commit('APP_TOTAL_USER_REQUEST');
      return applicationService
        .getTotalUser()
        .then(function(data) {
          commit('APP_TOTAL_USER_SUCCESS', data);
        })
        .catch(err => {
          commit('APP_TOTAL_USER_FAILURE');
          throw err;
        });
    },
    getUserActivity({ commit }, params) {
      commit('APP_USER_ACTIVITY_REQUEST');
      return applicationService
        .getUserActivity()
        .then(function(data) {
          commit('APP_USER_ACTIVITY_SUCCESS', data);
        })
        .catch(err => {
          commit('APP_USER_ACTIVITY_FAILURE');
          throw err;
        });
    },
    getAppActivity({ commit }, params) {
      commit('APP_ACTIVITY_REQUEST');
      return applicationService
        .getAppActivity()
        .then(function(data) {
          commit('APP_ACTIVITY_SUCCESS', data);
        })
        .catch(err => {
          commit('APP_ACTIVITY_FAILURE');
          throw err;
        });
    }
  },
  mutations: {
    APP_TOTAL_USER_REQUEST(state, params) {
      state.totalUser = null;
    },
    APP_TOTAL_USER_SUCCESS(state, data) {
      state.totalUser = data.data.farmer_count;
    },
    APP_TOTAL_USER_FAILURE(state, params) {
      state.totalUser = null;
    },
    APP_USER_ACTIVITY_REQUEST(state, params) {
      state.userActivity = null;
    },
    APP_USER_ACTIVITY_SUCCESS(state, data) {
      state.userActivity = data;
    },
    APP_USER_ACTIVITY_FAILURE(state, params) {
      state.userActivity = null;
    },
    APP_ACTIVITY_REQUEST(state, params) {
      state.appActivity = null;
    },
    APP_ACTIVITY_SUCCESS(state, data) {
      state.appActivity = data.data;
    },
    APP_ACTIVITY_FAILURE(state, params) {
      state.appActivity = null;
    }
  }
};

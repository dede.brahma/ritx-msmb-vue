import { farmerService } from '../../services/farmerService';

const initialState = {
  farmers: {
    params: null,
    farmers: null,
    commodities: {},
    fields: {},
    getAllField: {},
    getAllCommodity: {},
    active_farmer: {},
    commodity_plant: {},
    farmer_by_gender: {},
    farmer_by_status: {},
    farmer_by_location: {},
    registered_field: {},
    registered_commodity: {},
    registered_id: {},
    totalFarmerhasField: 0,
    totalRegisteredFarmer: 0,
    totalFarmerhasCommodity: 0,
    totalActiveFarmer: 0,
    meta: null,
    app_activities: []
  },
  detail: {
    id: null,
    farmer: null,
    fields: [],
    points: [],
    notifications: [],
    app_activities: []
  }
};

export default {
  namespaced: false,
  state: initialState,
  actions: {
    totalFarmerhasField({ commit }, params) {
      commit('TOTAL_FARMER_HAS_FIELD_REQUEST', params);
      return farmerService
        .totalFarmerhasField(params)
        .then(function(data) {
          commit('TOTAL_FARMER_HAS_FIELD_SUCCESS', data);
        })
        .catch(err => {
          commit('TOTAL_FARMER_HAS_FIELD_FAILURE');
          throw err;
        });
    },
    totalRegisteredFarmer({ commit }, params) {
      commit('TOTAL_REGISTERED_FARMER_REQUEST', params);
      return farmerService
        .totalRegisteredFarmer(params)
        .then(function(data) {
          commit('TOTAL_REGISTERED_FARMER_SUCCESS', data);
        })
        .catch(err => {
          commit('TOTAL_REGISTERED_FARMER_FAILURE');
          throw err;
        });
    },
    totalFarmerhasCommodity({ commit }, params) {
      commit('TOTAL_FARMER_HAS_COMMODITY_REQUEST', params);
      return farmerService
        .totalFarmerhasCommodity(params)
        .then(function(data) {
          commit('TOTAL_FARMER_HAS_COMMODITY_SUCCESS', data);
        })
        .catch(err => {
          commit('TOTAL_FARMER_HAS_COMMODITY_FAILURE');
          throw err;
        });
    },
    totalActiveFarmer({ commit }, params) {
      commit('TOTAL_ACTIVE_FARMER_REQUEST', params);
      return farmerService
        .totalActiveFarmer(params)
        .then(function(data) {
          commit('TOTAL_ACTIVE_FARMER_SUCCESS', data);
        })
        .catch(err => {
          commit('TOTAL_ACTIVE_FARMER_FAILURE');
          throw err;
        });
    },
    countRegisteredFarmer({ commit }, params) {
      commit('GET_COUNT_FARMER_REQUEST', params);
      return farmerService
        .countRegisteredFarmer(params)
        .then(function(data) {
          commit('GET_COUNT_FARMER_SUCCESS', data);
        })
        .catch(err => {
          commit('GET_COUNT_FARMER_FAILURE');
          throw err;
        });
    },
    countActiveFarmer({ commit }, params) {
      commit('GET_ACTIVE_FARMER_REQUEST', params);
      return farmerService
        .countActiveFarmer(params)
        .then(function(data) {
          commit('GET_ACTIVE_FARMER_SUCCESS', data);
        })
        .catch(err => {
          commit('GET_ACTIVE_FARMER_FAILURE');
          throw err;
        });
    },
    async countRegisteredField({ commit }, params) {
      commit('GET_REGISTERED_FIELD_REQUEST', params);
      return await farmerService
        .countRegisteredField(params)
        .then(function(data) {
          commit('GET_REGISTERED_FIELD_SUCCESS', data);
        })
        .catch(err => {
          commit('GET_REGISTERED_FIELD_FAILURE');
          throw err;
        });
    },
    countRegisteredCommodity({ commit }, params) {
      commit('GET_REGISTERED_COMMODITY_REQUEST', params);
      return farmerService
        .countRegisteredCommodity(params)
        .then(function(data) {
          commit('GET_REGISTERED_COMMODITY_SUCCESS', data);
        })
        .catch(err => {
          commit('GET_REGISTERED_COMMODITY_FAILURE');
          throw err;
        });
    },
    getFarmerByGender({ commit }, params) {
      commit('GET_FARMER_BY_GENDER_REQUEST', params);
      return farmerService
        .getFarmerByGender(params)
        .then(function(data) {
          commit('GET_FARMER_BY_GENDER_SUCCESS', data);
        })
        .catch(err => {
          commit('GET_FARMER_BY_GENDER_FAILURE');
          throw err;
        });
    },
    getAllCommodity({ commit }, params) {
      commit('GET_ALL_COMMODITY_REQUEST', params);
      return farmerService
        .getAllCommodity(params)
        .then(function(data) {
          commit('GET_ALL_COMMODITY_SUCCESS', data);
        })
        .catch(err => {
          commit('GET_ALL_COMMODITY_FAILURE');
          throw err;
        });
    },
    getAllField({ commit }, params) {
      commit('GET_ALL_FIELDS_REQUEST', params);
      return farmerService
        .getAllField(params)
        .then(function(data) {
          commit('GET_ALL_FIELDS_SUCCESS', data);
        })
        .catch(err => {
          commit('GET_ALL_FIELDS_FAILURE');
          throw err;
        });
    },
    getFarmerByStatus({ commit }, params) {
      commit('GET_FARMER_BY_STATUS_REQUEST', params);
      return farmerService
        .getFarmerByStatus(params)
        .then(function(data) {
          commit('GET_FARMER_BY_STATUS_SUCCESS', data);
        })
        .catch(err => {
          commit('GET_FARMER_BY_STATUS_FAILURE');
          throw err;
        });
    },
    countFarmerByLocation({ commit }, params) {
      commit('GET_FARMER_BY_LOCATION_REQUEST', params);
      return farmerService
        .countFarmerByLocation(params)
        .then(function(data) {
          commit('GET_FARMER_BY_LOCATION_SUCCESS', data);
        })
        .catch(err => {
          commit('GET_FARMER_BY_LOCATION_FAILURE');
          throw err;
        });
    },
    getCommodityPlant({ commit }, params) {
      commit('GET_COMMODITY_PLANT_REQUEST', params);
      return farmerService
        .getCommodityPlant(params)
        .then(function(data) {
          commit('GET_COMMODITY_PLANT_SUCCESS', data);
        })
        .catch(err => {
          commit('GET_COMMODITY_PLANT_FAILURE');
          throw err;
        });
    },
    getDetailFarmer({ commit }, params) {
      commit('GET_DETAIL_FARMER_REQUEST', params);
      return farmerService
        .getDetailFarmer(params)
        .then(function(data) {
          commit('GET_DETAIL_FARMER_SUCCESS', data);
        })
        .catch(err => {
          commit('GET_DETAIL_FARMER_FAILURE');
          throw err;
        });
    },
    getFieldsByFarmerId({ commit }, params) {
      commit('GET_FIELDS_FARMER_REQUEST', params);
      return farmerService
        .getFieldsByFarmerId(params)
        .then(function(data) {
          commit('GET_FIELDS_FARMER_SUCCESS', data);
        })
        .catch(err => {
          commit('GET_FIELDS_FARMER_FAILURE');
          throw err;
        });
    },
    getFarmerPoint({ commit }, params) {
      commit('GET_FARMER_POINT_REQUEST', params);
      return farmerService
        .getFarmerPoint(params)
        .then(function(data) {
          commit('GET_FARMER_POINT_SUCCESS', data);
        })
        .catch(err => {
          commit('GET_FARMER_POINT_FAILURE');
          throw err;
        });
    },
    getFarmerNotif({ commit }, params) {
      commit('GET_FARMER_NOTIFICATION_REQUEST', params);
      return farmerService
        .getFarmerNotif(params)
        .then(function(data) {
          commit('GET_FARMER_NOTIFICATION_SUCCESS', data);
        })
        .catch(err => {
          commit('GET_FARMER_NOTIFICATION_FAILURE');
          throw err;
        });
    },
    getAppActivities({ commit }, params) {
      commit('GET_APP_ACTIVITIES_REQUEST', params);
      return farmerService
        .getAppActivities(params)
        .then(function(data) {
          commit('GET_APP_ACTIVITIES_SUCCESS', data);
        })
        .catch(err => {
          commit('GET_APP_ACTIVITIES_FAILURE');
          throw err;
        });
    },
    getFarmerAppActivities({ commit }, params) {
      commit('GET_FARMER_APP_ACTIVITIES_REQUEST', params);
      return farmerService
        .getFarmerAppActivities(params)
        .then(function(data) {
          commit('GET_FARMER_APP_ACTIVITIES_SUCCESS', data);
        })
        .catch(err => {
          commit('GET_FARMER_APP_ACTIVITIES_FAILURE');
          throw err;
        });
    }
  },
  mutations: {
    TOTAL_FARMER_HAS_FIELD_REQUEST(state, params) {
      state.farmers.totalFarmerhasField = null;
    },
    TOTAL_FARMER_HAS_FIELD_SUCCESS(state, data) {
      state.farmers.totalFarmerhasField = data.farmer_count;
    },
    TOTAL_FARMER_HAS_FIELD_FAILURE(state, params) {
      state.farmers.totalFarmerhasField = null;
    },
    TOTAL_REGISTERED_FARMER_REQUEST(state, params) {
      state.farmers.totalRegisteredFarmer = null;
    },
    TOTAL_REGISTERED_FARMER_SUCCESS(state, data) {
      state.farmers.totalRegisteredFarmer = data.farmer_count;
    },
    TOTAL_REGISTERED_FARMER_FAILURE(state, params) {
      state.farmers.totalRegisteredFarmer = null;
    },
    TOTAL_FARMER_HAS_COMMODITY_REQUEST(state, params) {
      state.farmers.totalFarmerhasCommodity = null;
    },
    TOTAL_FARMER_HAS_COMMODITY_SUCCESS(state, data) {
      state.farmers.totalFarmerhasCommodity = data.farmer_count;
    },
    TOTAL_FARMER_HAS_COMMODITY_FAILURE(state, params) {
      state.farmers.totalFarmerhasCommodity = null;
    },
    TOTAL_ACTIVE_FARMER_REQUEST(state, params) {
      state.farmers.totalActiveFarmer = null;
    },
    TOTAL_ACTIVE_FARMER_SUCCESS(state, data) {
      state.farmers.totalActiveFarmer = data.farmer_count;
    },
    TOTAL_ACTIVE_FARMER_FAILURE(state, params) {
      state.farmers.params = params;
      state.farmers.totalActiveFarmer = null;
    },
    GET_COUNT_FARMER_REQUEST(state, params) {
      state.farmers.params = params;
      state.farmers.registered_id = null;
    },
    GET_COUNT_FARMER_SUCCESS(state, data) {
      state.farmers.registered_id = data.data;
    },
    GET_COUNT_FARMER_FAILURE(state, params) {
      state.farmers.params = params;
      state.farmers.registered_id = null;
    },
    GET_ACTIVE_FARMER_REQUEST(state, params) {
      state.farmers.params = params;
      state.farmers.active_farmer = null;
    },
    GET_ACTIVE_FARMER_SUCCESS(state, data) {
      state.farmers.active_farmer = data.data;
    },
    GET_ACTIVE_FARMER_FAILURE(state, params) {
      state.farmers.params = params;
      state.farmers.active_farmer = null;
    },
    GET_REGISTERED_FIELD_REQUEST(state, params) {
      state.farmers.params = params;
      state.farmers.registered_field = null;
    },
    GET_REGISTERED_FIELD_SUCCESS(state, data) {
      state.farmers.registered_field = data.data;
    },
    GET_REGISTERED_FIELD_FAILURE(state, params) {
      state.farmers.params = params;
      state.farmers.registered_field = null;
    },
    GET_REGISTERED_COMMODITY_REQUEST(state, params) {
      state.farmers.params = params;
      state.farmers.registered_commodity = null;
    },
    GET_REGISTERED_COMMODITY_SUCCESS(state, data) {
      state.farmers.registered_commodity = data.data;
    },
    GET_REGISTERED_COMMODITY_FAILURE(state, params) {
      state.farmers.params = params;
      state.farmers.registered_commodity = null;
    },
    GET_ALL_COMMODITY_REQUEST(state, params) {
      state.farmers.params = params;
      state.farmers.getAllCommodity = null;
    },
    GET_ALL_COMMODITY_SUCCESS(state, data) {
      state.farmers.getAllCommodity = data.commodity_count;
    },
    GET_ALL_COMMODITY_FAILURE(state, params) {
      state.farmers.params = params;
      state.farmers.getAllCommodity = null;
    },
    GET_ALL_FIELDS_REQUEST(state, params) {
      state.farmers.getAllField = null;
    },
    GET_ALL_FIELDS_SUCCESS(state, data) {
      state.farmers.getAllField = data.field_count;
    },
    GET_ALL_FIELDS_FAILURE(state, params) {
      state.farmers.getAllField = null;
    },
    GET_FARMER_BY_GENDER_REQUEST(state, params) {
      state.farmers.params = params;
      state.farmers.farmer_by_gender = null;
    },
    GET_FARMER_BY_GENDER_SUCCESS(state, data) {
      state.farmers.farmer_by_gender = data.data;
    },
    GET_FARMER_BY_GENDER_FAILURE(state, params) {
      state.farmers.params = params;
      state.farmers.farmer_by_gender = null;
    },
    GET_FARMER_BY_STATUS_REQUEST(state, params) {
      state.farmers.params = params;
      state.farmers.farmer_by_status = null;
    },
    GET_FARMER_BY_STATUS_SUCCESS(state, data) {
      state.farmers.farmer_by_status = data.data;
    },
    GET_FARMER_BY_STATUS_FAILURE(state, params) {
      state.farmers.params = params;
      state.farmers.farmer_by_status = null;
    },
    GET_FARMER_BY_LOCATION_REQUEST(state, params) {
      state.farmers.params = params;
      state.farmers.farmer_by_location = null;
    },
    GET_FARMER_BY_LOCATION_SUCCESS(state, data) {
      state.farmers.farmer_by_location = data.data;
    },
    GET_FARMER_BY_LOCATION_FAILURE(state, params) {
      state.farmers.params = params;
      state.farmers.farmer_by_location = null;
    },
    GET_COMMODITY_PLANT_REQUEST(state, params) {
      state.farmers.params = params;
      state.farmers.commodity_plant = null;
    },
    GET_COMMODITY_PLANT_SUCCESS(state, data) {
      state.farmers.commodity_plant = data.data;
    },
    GET_COMMODITY_PLANT_FAILURE(state, params) {
      state.farmers.params = params;
      state.farmers.commodity_plant = null;
    },
    GET_DETAIL_FARMER_REQUEST(state, params) {
      state.detail.id = params;
      state.detail.farmer = null;
    },
    GET_DETAIL_FARMER_SUCCESS(state, data) {
      state.detail.farmer = data.data;
    },
    GET_DETAIL_FARMER_FAILURE(state, params) {
      state.detail.id = params;
      state.detail.farmer = null;
    },
    GET_FIELDS_FARMER_REQUEST(state, params) {
      state.detail.fields = null;
    },
    GET_FIELDS_FARMER_SUCCESS(state, data) {
      state.detail.fields = data;
    },
    GET_FIELDS_FARMER_FAILURE(state, params) {
      state.detail.fields = null;
    },
    GET_FARMER_POINT_REQUEST(state, params) {
      state.detail.points = null;
    },
    GET_FARMER_POINT_SUCCESS(state, data) {
      state.detail.points = data;
    },
    GET_FARMER_POINT_FAILURE(state, params) {
      state.detail.points = null;
    },
    GET_FARMER_NOTIFICATION_REQUEST(state, params) {
      state.detail.notifications = null;
    },
    GET_FARMER_NOTIFICATION_SUCCESS(state, data) {
      state.detail.notifications = data;
    },
    GET_FARMER_NOTIFICATION_FAILURE(state, params) {
      state.detail.notifications = null;
    },
    GET_APP_ACTIVITIES_REQUEST(state, params) {
      state.farmers.app_activities = null;
    },
    GET_APP_ACTIVITIES_SUCCESS(state, data) {
      state.farmers.app_activities = data;
    },
    GET_APP_ACTIVITIES_FAILURE(state, params) {
      state.farmers.app_activities = null;
    },
    GET_FARMER_APP_ACTIVITIES_REQUEST(state, params) {
      state.detail.app_activities = null;
    },
    GET_FARMER_APP_ACTIVITIES_SUCCESS(state, data) {
      state.detail.app_activities = data;
    },
    GET_FARMER_APP_ACTIVITIES_FAILURE(state, params) {
      state.detail.app_activities = null;
    }
  },
  getters: {
    active_farmers: state => (state.farmers.active_farmer ? state.farmers.active_farmer.farmer_count : 0),
    commodity_farmers: state =>
      state.farmers.registered_commodity ? state.farmers.registered_commodity.farmer_count : 0,
    registered_field: state => (state.farmers.registered_field ? state.farmers.registered_field.farmer_count : 0),
    registered_id: state => (state.farmers.registered_id ? state.farmers.registered_id.farmer_count : 0),
    status_farmer: state => [
      state.farmers.registered_id,
      state.farmers.registered_field,
      state.farmers.registered_commodity,
      state.farmers.active_farmer
    ],
    registered_gender(state) {
      const gender = state.farmers.farmer_by_gender ? Object.values(state.farmers.farmer_by_gender) : [];
      return gender;
    },
    registered_location(state) {
      const location = state.farmers.farmer_by_location ? Object.values(state.farmers.farmer_by_location) : [];
      return location;
    },
    commodities(state) {
      const commodity = state.farmers.commodity_plant ? Object.values(state.farmers.commodity_plant) : [];
      return commodity;
    }
  }
};

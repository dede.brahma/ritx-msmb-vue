import { articleService } from '../../services/articleService';

const initialState = {
  topten: []
};

export default {
  namespaced: false,
  state: initialState,
  actions: {
    topTenArticle({ commit }, params) {
      commit('TOP_TEN_REQUEST');
      return articleService
        .topTenArticle(params)
        .then(function(data) {
          commit('TOP_TEN_SUCCESS', data);
        })
        .catch(err => {
          commit('TOP_TEN_FAILURE');
          throw err;
        });
    }
  },
  mutations: {
    TOP_TEN_REQUEST(state, params) {
      state.topten = null;
    },
    TOP_TEN_SUCCESS(state, data) {
      state.topten = data.data;
    },
    TOP_TEN_FAILURE(state, params) {
      state.topten = null;
    }
  }
};

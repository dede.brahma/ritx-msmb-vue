import { intermediasiService } from '../../services/intermediasiService';

const initialState = {
  intermediasiGender: null,
  intermediasiLocation: null,
  intermediasiRole: null,
  intermediasiRoleGroup: null
};

export default {
  namespaced: false,
  state: initialState,
  actions: {
    intermediasiGender({ commit }, params) {
      commit('INTERMEDIASI_GENDER_REQUEST');
      return intermediasiService
        .intermediasiGender(params)
        .then(function(data) {
          commit('INTERMEDIASI_GENDER_SUCCESS', data);
        })
        .catch(err => {
          commit('INTERMEDIASI_GENDER_FAILURE');
          throw err;
        });
    },
    intermediasiLocation({ commit }, params) {
      commit('ACCESSED_FORUM_REQUEST');
      return intermediasiService
        .intermediasiLocation(params)
        .then(function(data) {
          commit('ACCESSED_FORUM_SUCCESS', data);
        })
        .catch(err => {
          commit('ACCESSED_FORUM_FAILURE');
          throw err;
        });
    },
    intermediasiRole({ commit }, params) {
      commit('SHARED_POST_REQUEST');
      return intermediasiService
        .intermediasiRole(params)
        .then(function(data) {
          commit('SHARED_POST_SUCCESS', data);
        })
        .catch(err => {
          commit('SHARED_POST_FAILURE');
          throw err;
        });
    },
    intermediasiRoleGroup({ commit }, params) {
      commit('INTERMEDIASI_ROLE_GROUP_REQUEST');
      return intermediasiService
        .intermediasiRoleGroup(params)
        .then(function(data) {
          commit('INTERMEDIASI_ROLE_GROUP_SUCCESS', data);
        })
        .catch(err => {
          commit('INTERMEDIASI_ROLE_GROUP_FAILURE');
          throw err;
        });
    }
  },
  mutations: {
    INTERMEDIASI_GENDER_REQUEST(state, params) {
      state.intermediasiGender = null;
    },
    INTERMEDIASI_GENDER_SUCCESS(state, data) {
      state.intermediasiGender = data.data;
    },
    INTERMEDIASI_GENDER_FAILURE(state, params) {
      state.intermediasiGender = null;
    },
    ACCESSED_FORUM_REQUEST(state, params) {
      state.intermediasiLocation = null;
    },
    ACCESSED_FORUM_SUCCESS(state, data) {
      state.intermediasiLocation = data.data;
    },
    ACCESSED_FORUM_FAILURE(state, params) {
      state.intermediasiLocation = null;
    },
    SHARED_POST_REQUEST(state, params) {
      state.intermediasiRole = null;
    },
    SHARED_POST_SUCCESS(state, data) {
      state.intermediasiRole = data.data;
    },
    SHARED_POST_FAILURE(state, params) {
      state.intermediasiRole = null;
    },
    INTERMEDIASI_ROLE_GROUP_REQUEST(state, params) {
      state.intermediasiRoleGroup = null;
    },
    INTERMEDIASI_ROLE_GROUP_SUCCESS(state, data) {
      state.intermediasiRoleGroup = data.data;
    },
    INTERMEDIASI_ROLE_GROUP_FAILURE(state, params) {
      state.intermediasiRoleGroup = null;
    }
  },
  getters: {
    intermediasiLocation(state) {
      const location = state.intermediasiLocation ? Object.values(state.intermediasiLocation) : [];
      return location;
    }
  }
};

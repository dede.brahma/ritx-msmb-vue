import { recommendationService } from '../../services/recommendationService';

const initialState = {
  recommendationReport: null
}

export default {
  namespaced: false,
  state: initialState,
  actions: {
    getRecommendationReport({ commit }, params) {
      commit("RECOMMENDATION_REPORT_REQUEST");
      return recommendationService
        .getRecommendationReport(params)
        .then(function(data) {
          commit("RECOMMENDATION_REPORT_SUCCESS", data);
        })
        .catch(err => {
          commit("RECOMMENDATION_REPORT_FAILURE");
          throw err;
        });
    }
  },
  mutations: {
    RECOMMENDATION_REPORT_REQUEST(state, params) {
      state.recommendationReport = null;
    },
    RECOMMENDATION_REPORT_SUCCESS(state, data) {
      state.recommendationReport = data.data;
    },
    RECOMMENDATION_REPORT_FAILURE(state, params) {
      state.recommendationReport = null;
    }
  }
}


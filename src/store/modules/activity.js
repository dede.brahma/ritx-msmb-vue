import { activityService } from '../../services/activityService';

const initialState = {
  activities: null,
  totalActiveCount: 0,
  countActiveUser: 0
};

export default {
  namespaced: false,
  state: initialState,
  actions: {
    getUserActivityReport({ commit }, params) {
      commit("USER_ACTIVITY_REPORT_REQUEST");
      return activityService
        .getUserActivityReport(params)
        .then(function(data) {
          commit('USER_ACTIVITY_REPORT_SUCCESS', data);
        })
        .catch(err => {
          commit('USER_ACTIVITY_REPORT_FAILURE');
          throw err;
        })
    },
    getTotalActiveCount({ commit }, params) {
      commit("TOTAL_ACTIVE_COUNT_REQUEST");
      return activityService
        .getTotalActiveUserCount(params)
        .then(function(data) {
          commit('TOTAL_ACTIVE_COUNT_SUCCESS', data);
        })
        .catch(err => {
          commit('TOTAL_ACTIVE_COUNT_FAILURE');
          throw err;
        })
    },
    getCountActiveUser({ commit }, params) {
      commit("COUNT_ACTIVE_USER_REQUEST");
      return activityService
        .getCountActiveUser(params)
        .then(function(data) {
          commit('COUNT_ACTIVE_USER_SUCCESS', data);
        })
        .catch(err => {
          commit('COUNT_ACTIVE_USER_FAILURE');
          throw err;
        })
    }
  },
  mutations: {
    USER_ACTIVITY_REPORT_REQUEST(state, params) {
      state.activities = null;
    },
    USER_ACTIVITY_REPORT_SUCCESS(state, data) {
      state.activities = data;
    },
    USER_ACTIVITY_REPORT_FAILURE(state, params) {
      state.activities = null;
    },
    TOTAL_ACTIVE_COUNT_REQUEST(state, params) {
      state.totalActiveCount = 0;
    },
    TOTAL_ACTIVE_COUNT_SUCCESS(state, data) {
      state.totalActiveCount = data.total_active_user;
    },
    TOTAL_ACTIVE_COUNT_FAILURE(state, params) {
      state.totalActiveCount = 0;
    },
    COUNT_ACTIVE_USER_REQUEST(state, params) {
      state.countActiveUser = null;
    },
    COUNT_ACTIVE_USER_SUCCESS(state, data) {
      state.countActiveUser = data.data[0].count;
    },
    COUNT_ACTIVE_USER_FAILURE(state, params) {
      state.countActiveUser = null;
    }
  }
}

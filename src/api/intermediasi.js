import request from '@/utils/request'

export function intermediasiGender(query) {
  return request({
    url: '/api/reporting/v1/intermediasi/gender?',
    method: 'get',
    params: query
  });
}

export function intermediasiLocation(query) {
  return request({
    url: '/api/reporting/v1/intermediasi/city?',
    method: 'get',
    params: query
  });
}

export function intermediasiRole(query) {
  return request({
    url: '/api/reporting/v1/intermediasi/role?',
    method: 'get',
    params: query
  });
}

export function intermediasiRoleGroup(query) {
  return request({
    url: '/api/reporting/v1/intermediasi/role-group?',
    method: 'get',
    params: query
  });
}

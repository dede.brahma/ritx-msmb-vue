import request from '@/utils/request'

export function getUserActivityReport(query) {
  return request({
    url: `/api/reporting/v1/activity/user`,
    method: 'get',
    params: query
  })
}

export function getTotalUserCount(query) {
  return request({
    url: `/api/reporting/v1/activity/total-user`,
    method: 'get',
    params: query
  })
}

export function getTotalActiveUserCount(query) {
  return request({
    url: `/api/reporting/v1/activity/total-active-user`,
    method: 'get',
    params: query
  })
}

export function getCountActiveUser(query) {
  return request({
    url: '/api/reporting/v1/activity/count-active-user',
    method: 'get',
    params: query
  });
}

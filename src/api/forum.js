import request from '@/utils/request'

export function countPost(params) {
  return request({
    url: '/api/reporting/v1/forum/count/post',
    method: 'get',
    params: params
  });
}

export function countAccessedForum(params) {
  return request({
    url: '/api/reporting/v1/forum/count/accessed-forum',
    method: 'get',
    params: params
  });
}

export function countSharedPost(params) {
  return request({
    url: '/api/reporting/v1/forum/count/shared-post',
    method: 'get',
    params: params
  });
}

export function countComment(params) {
  return request({
    url: '/api/reporting/v1/forum/count/comment',
    method: 'get',
    params: params
  });
}

export function countForumTopic(params) {
  return request({
    url: '/api/reporting/v1/forum/count/topic',
    method: 'get',
    params: params
  });
}

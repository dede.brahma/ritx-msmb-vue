import request from '@/utils/request';

export function totalRegisteredFarmer(query) {
  return request({
    url: '/api/reporting/v1/statistic/farmer/total-register?',
    method: 'get',
    params: query
  });
}
export function totalFarmerhasField(query) {
  return request({
    url: '/api/reporting/v1/statistic/farmer/total-has-field?',
    method: 'get',
    params: query
  });
}
export function totalFarmerhasCommodity(query) {
  return request({
    url: '/api/reporting/v1/statistic/farmer/total-has-commodity?',
    method: 'get',
    params: query
  });
}
export function totalActiveFarmer(query) {
  return request({
    url: '/api/reporting/v1/statistic/farmer/total-active?',
    method: 'get',
    params: query
  });
}

export function countRegisteredId(query) {
  return request({
    url: '/api/reporting/v1/statistic/farmer/register-id',
    method: 'get',
    params: query
  });
}
export function countActiveFarmer(query) {
  return request({
    url: '/api/reporting/v1/statistic/farmer/active',
    method: 'get',
    params: query
  });
}
export function countRegisteredField(query) {
  return request({
    url: '/api/reporting/v1/statistic/farmer/register-field',
    method: 'get',
    params: query
  });
}
export function countRegisteredCommodity(query) {
  return request({
    url: '/api/reporting/v1/statistic/farmer/register-commodity',
    method: 'get',
    params: query
  });
}
export function getFarmerByGender(query) {
  return request({
    url: '/api/reporting/v1/statistic/farmer/gender',
    method: 'get',
    params: query
  });
}
export function getCommodityPlant(query) {
  return request({
    url: '/api/reporting/v1/statistic/farmer/commodity-plant',
    method: 'get',
    params: query
  });
}
export function getAllCommodity(query) {
  return request({
    url: '/api/reporting/v1/statistic/commodity',
    method: 'get',
    params: query
  });
}
export function getAllField(query) {
  return request({
    url: '/api/reporting/v1/statistic/field',
    method: 'get',
    params: query
  });
}
export function getFarmerByStatus(query) {
  return request({
    url: '/api/reporting/v1/statistic/farmer/status',
    method: 'get',
    params: query
  });
}

export function countFarmerByLocation(query) {
  return request({
    url: '/api/reporting/v1/statistic/farmer/location',
    method: 'get',
    params: query
  });
}

export function getAllFarmer(query) {
  return request({
    url: '/api/reporting/v1/farmer/list',
    method: 'get',
    params: query
  });
}

export function getDetailFarmer(id) {
  return request({
    url: `/api/reporting/v1/farmer/${id}`,
    method: 'get'
  });
}

export function getFieldsByFarmerId(id) {
  return request({
    url: `/api/reporting/v1/farmer/field/${id}`,
    method: 'get'
  });
}

export function getCommodityByField(id, id_field, is_planting_plan = 0) {
  return request({
    url: `/api/reporting/v1/farmer/commodity?id_farmer=${id}&id_field=${id_field}&is_planting_plan=${is_planting_plan}`,
    method: 'get'
  });
}

export function getFarmerPoint(id) {
  return request({
    url: `/api/reporting/v1/farmer/point/${id}`,
    method: 'get'
  });
}

export function getFarmerNotif(id) {
  return request({
    url: `/api/reporting/v1/farmer/push-notification/${id}`,
    method: 'get'
  });
}
export function getAppActivities() {
  return request({
    url: `/api/reporting/v1/statistic/app-activity`,
    method: 'get'
  });
}
export function getFarmerAppActivities(id) {
  return request({
    url: `/api/reporting/v1/farmer/activity-apps/${id}`,
    method: 'get'
  });
}

import request from '@/utils/request'

export function getTotalUser() {
  return request({
    url: '/api/reporting/v1/activity/total-user',
    method: 'get'
  });
}

export function getUserActivity() {
  return request({
    url: '/api/reporting/v1/activity/user',
    method: 'get'
  });
}

export function getAppActivity() {
  return request({
    url: '/api/reporting/v1/statistic/app-activity',
    method: 'get'
  });
}

import request from '@/utils/request'

export function getRecommendationReport(params) {
  return request({
    url: `/api/reporting/v1/recommendation`,
    method: 'get',
    params: params
  });
}

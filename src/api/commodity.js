import request from '@/utils/request';

export function getCommodities(query) {
  return request({
    url: `/api/reporting/v1/commodity/search?param=${query}`,
    method: 'get'
  });
}

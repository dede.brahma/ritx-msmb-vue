import request from '@/utils/request';

export function getCities(query) {
  return request({
    url: `/api/reporting/v1/city/search?param=${query}`,
    method: 'get'
  });
}

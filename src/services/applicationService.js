import * as application from '../api/application';

export const applicationService = {
  getTotalUser,
  getUserActivity,
  getAppActivity
};

async function getTotalUser() {
  try {
    const response = await application.getTotalUser();
    return response.data
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}

async function getUserActivity() {
  try {
    const response = await application.getUserActivity();
    return response.data
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}

async function getAppActivity() {
  try {
    const response = await application.getAppActivity();
    return response.data
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}

import * as recommendation from '../api/recommendation';

export const recommendationService = {
  getRecommendationReport
};

async function getRecommendationReport(params) {
  try {
    const response = await recommendation.getRecommendationReport(params);
    return response.data;
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}

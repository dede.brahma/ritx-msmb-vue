import * as activity from '../api/activity';

export const activityService = {
  getUserActivityReport,
  getTotalUserCount,
  getTotalActiveUserCount,
  getCountActiveUser
};

async function getUserActivityReport(params) {
  try {
    const response = await activity.getUserActivityReport(params);
    return response.data.data;
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}

async function getTotalUserCount(params) {
  try {
    const response = await activity.getTotalUserCount(params);
    return response.data.data;
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}

async function getTotalActiveUserCount(params) {
  try {
    const response = await activity.getTotalActiveUserCount(params);
    return response.data.data;
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}

async function getCountActiveUser(params) {
  try {
    const response = await activity.getCountActiveUser(params);
    return response.data;
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}



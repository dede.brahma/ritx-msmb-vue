import * as forum from '../api/forum';

export const forumService = {
  countPost,
  countAccessedForum,
  countSharedPost,
  countComment,
  countForumTopic
};

async function countPost(params) {
  try {
    const response = await forum.countPost(params);
    return response.data
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}

async function countAccessedForum(params) {
  try {
    const response = await forum.countAccessedForum(params);
    return response.data
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}

async function countSharedPost(params) {
  try {
    const response = await forum.countSharedPost(params);
    return response.data
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}

async function countComment(params) {
  try {
    const response = await forum.countComment(params);
    return response.data
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}

async function countForumTopic(params) {
  try {
    const response = await forum.countForumTopic(params);
    return response.data
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}


import * as intermediasi from '../api/intermediasi';

export const intermediasiService = {
  intermediasiGender,
  intermediasiLocation,
  intermediasiRole,
  intermediasiRoleGroup
};

async function intermediasiGender(params) {
  try {
    const response = await intermediasi.intermediasiGender(params);
    return response.data
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}

async function intermediasiLocation(params) {
  try {
    const response = await intermediasi.intermediasiLocation(params);
    return response.data
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}

async function intermediasiRole(params) {
  try {
    const response = await intermediasi.intermediasiRole(params);
    return response.data
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}

async function intermediasiRoleGroup(query) {
  try {
    const response = await intermediasi.intermediasiRoleGroup(query);
    return response.data
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}

import * as article from '../api/article';

export const articleService = {
  topTenArticle
};

async function topTenArticle(params) {
  try {
    const response = await article.topTenArticle(params);
    return response.data
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}

import * as farmer from '../api/farmer';

export const farmerService = {
  countRegisteredFarmer,
  countActiveFarmer,
  countRegisteredField,
  countRegisteredCommodity,
  getFarmerByGender,
  getCommodityPlant,
  getFarmerByStatus,
  getAllCommodity,
  getAllField,
  countFarmerByLocation,
  getDetailFarmer,
  getFieldsByFarmerId,
  getFarmerPoint,
  getFarmerNotif,
  getAppActivities,
  getFarmerAppActivities,
  totalRegisteredFarmer,
  totalFarmerhasField,
  totalFarmerhasCommodity,
  totalActiveFarmer
};
// get farmer by registered ID
async function totalRegisteredFarmer(params) {
  try {
    const response = await farmer.totalRegisteredFarmer(params);
    return response.data.data
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}
async function totalFarmerhasField(params) {
  try {
    const response = await farmer.totalFarmerhasField(params);
    return response.data.data
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}
async function totalFarmerhasCommodity(params) {
  try {
    const response = await farmer.totalFarmerhasCommodity(params);
    return response.data.data
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}
async function totalActiveFarmer(params) {
  try {
    const response = await farmer.totalActiveFarmer(params);
    return response.data.data
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}

// get farmer by registered ID
async function countRegisteredFarmer(params) {
  try {
    const response = await farmer.countRegisteredId(params);
    const data = response.data;
    return data;
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}

// get active farmer
async function countActiveFarmer(params) {
  try {
    const response = await farmer.countActiveFarmer(params);
    const data = response.data;
    return data;
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}
async function countRegisteredField(params) {
  try {
    const response = await farmer.countRegisteredField(params);
    const data = await response.data;
    return data;
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}
async function countRegisteredCommodity(params) {
  try {
    const response = await farmer.countRegisteredCommodity(params);
    const data = response.data;
    return data;
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}
async function getFarmerByGender(params) {
  try {
    const response = await farmer.getFarmerByGender(params);
    const data = response.data;
    return data;
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}
async function getAllCommodity(params) {
  try {
    const response = await farmer.getAllCommodity(params);
    return response.data.data
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}
async function getAllField(params) {
  try {
    const response = await farmer.getAllField(params);
    return response.data.data
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}
async function getCommodityPlant(params) {
  try {
    const response = await farmer.getCommodityPlant(params);
    const data = response.data;
    return data;
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}
async function getFarmerByStatus(params) {
  try {
    const response = await farmer.getFarmerByStatus(params);
    const data = response.data;
    return data;
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}
async function countFarmerByLocation(params) {
  try {
    const response = await farmer.countFarmerByLocation(params);
    const data = response.data;
    return data;
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}
async function getDetailFarmer(params) {
  try {
    const response = await farmer.getDetailFarmer(params);
    const data = response.data;
    return data;
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}
async function getFieldsByFarmerId(params) {
  try {
    const response = await farmer.getFieldsByFarmerId(params);
    const data = response.data;
    var list = [];

    data.data.map(async function(field, key) {
      var commodities = await getCommodityByField(params, field.id, 0);
      if (commodities.is_planting_plan) {
        commodities = await getCommodityByField(params, field.id, 1);
      }
      field.commodities = commodities;
      list.push(field);
    });
    return list;
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}

async function getCommodityByField(params, field_id, is_planting_plan) {
  try {
    const response = await farmer.getCommodityByField(params, field_id, is_planting_plan);
    return response.data.data;
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}

async function getFarmerPoint(params) {
  try {
    const response = await farmer.getFarmerPoint(params);
    return response.data.data;
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}

async function getFarmerNotif(params) {
  try {
    const response = await farmer.getFarmerNotif(params);
    return response.data.data;
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}

async function getAppActivities() {
  try {
    const response = await farmer.getAppActivities();
    return response.data.data;
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}

async function getFarmerAppActivities(params) {
  try {
    const response = await farmer.getFarmerAppActivities(params);
    return response.data.data;
  } catch (err) {
    console.log(err);
    throw err.response;
  }
}

import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

/* Layout */
import Layout from '@/views/layout/Layout';

/* Router Modules */
// import componentsRouter from './modules/components';
// import chartsRouter from './modules/charts';
// import tableRouter from './modules/table';
// import nestedRouter from './modules/nested';

/** note: Submenu only appear when children.length>=1
 *  detail see  https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 **/

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirect in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    roles: ['admin','editor']    will control the page roles (you can set multiple roles)
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    noCache: true                if true, the page will no be cached(default is false)
    breadcrumb: false            if false, the item will hidden in breadcrumb(default is true)
  }
**/
export const constantRouterMap = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path*',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/authredirect'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/errorPage/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/errorPage/401'),
    hidden: true
  },
  {
    path: '',
    component: Layout,
    redirect: 'dashboard',
    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/dashboard/index'),
        name: 'Dashboard',
        meta: { title: 'Petani', icon: 'farmer', noCache: true }
      },
      {
        path: 'detail-farmer/:path',
        component: () => import('@/views/detail-farmer/index'),
        name: 'DetailFarmer',
        meta: { title: 'Detail Petani', icon: 'farmer', noCache: true },
        hidden: true
      },
      {
        path: 'aplikasi',
        component: () => import('@/views/aplikasi/index'),
        name: 'Aplikasi',
        meta: { title: 'Aplikasi', icon: 'aktifitas-white', noCache: true },
        hidden: true
      },
      {
        path: 'article',
        component: () => import('@/views/article/index'),
        name: 'Artikel',
        meta: { title: 'Artikel', icon: 'artikel', noCache: true },
        hidden: true
      },
      {
        path: 'forum',
        component: () => import('@/views/forum/index'),
        name: 'Forum',
        meta: { title: 'Forum', icon: 'forum', noCache: true },
        hidden: true
      },
      {
        path: 'intermediasi',
        component: () => import('@/views/intermediasi/index'),
        name: 'Intermediasi',
        meta: { title: 'Intermediasi', icon: 'group', noCache: true },
        hidden: true
      },
      {
        path: 'recommendation',
        component: () => import('@/views/recommendation/index'),
        name: 'Recommendation',
        meta: { title: 'Recommendation', icon: 'group', noCache: true },
        hidden: true
      }
    ]
  },
  {
    path: 'aplikasi',
    component: Layout,
    redirect: 'aplikasi',
    children: [
      {
        path: '',
        component: () => import('@/views/aplikasi/index'),
        name: 'Aplikasi',
        meta: { title: 'Aplikasi', icon: 'aktifitas-white', noCache: true }
      }
    ]
  },
  {
    path: 'article',
    component: Layout,
    redirect: 'article',
    children: [
      {
        path: '',
        component: () => import('@/views/article/index'),
        name: 'Artikel',
        meta: { title: 'Artikel', icon: 'artikel', noCache: true }
      }
    ]
  },
  {
    path: 'forum',
    component: Layout,
    redirect: 'forum',
    children: [
      {
        path: '',
        component: () => import('@/views/forum/index'),
        name: 'Forum',
        meta: { title: 'Forum', icon: 'forum', noCache: true }
      }
    ]
  },
  {
    path: 'intermediasi',
    component: Layout,
    redirect: 'intermediasi',
    children: [
      {
        path: '',
        component: () => import('@/views/intermediasi/index'),
        name: 'Intermediasi',
        meta: { title: 'Intermediasi', icon: 'group', noCache: true }
      }
    ]
  },
  {
    path: 'recommendation',
    component: Layout,
    redirect: 'recommendation',
    children: [
      {
        path: '',
        component: () => import('@/views/recommendation/index'),
        name: 'Recommendation',
        meta: { title: 'Recommendation', icon: 'group', noCache: true }
      }
    ]
  }
];

export default new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
});

export const asyncRouterMap = [
  /* {
    path: '/permission',
    component: Layout,
    redirect: '/permission/index',
    alwaysShow: true, // will always show the root menu
    meta: {
      title: 'Aplikasi',
      icon: 'application',
      roles: ['admin', 'editor'] // you can set roles in root nav
    },
    children: [
      {
        path: 'page',
        component: () => import('@/views/permission/page'),
        name: 'PagePermission',
        meta: {
          title: 'pagePermission',
          roles: ['admin'] // or you can only set roles in sub nav
        }
      },
      {
        path: 'directive',
        component: () => import('@/views/permission/directive'),
        name: 'DirectivePermission',
        meta: {
          title: 'directivePermission'
          // if do not set roles, means: this page does not require permission
        }
      }
    ]
  },

  {
    path: '/intermediasi',
    component: Layout,
    children: [
      {
        path: 'IntermediationPage',
        component: () => import('@/views/svg-icons/index'),
        name: 'icon',
        meta: { title: 'Intermediasi', icon: 'intermediation', noCache: true }
      }
    ]
  },
  {
    path: '/forum',
    component: Layout,
    children: [
      {
        path: 'index',
        component: () => import('@/views/svg-icons/index'),
        name: 'ForumPage',
        meta: { title: 'Forum', icon: 'list', noCache: true }
      }
    ]
  },
  {
    path: '/chatbot',
    component: Layout,
    children: [
      {
        path: 'index',
        component: () => import('@/views/svg-icons/index'),
        name: 'ChatBotPage',
        meta: { title: 'Chatbot', icon: 'chat-bot', noCache: true }
      }
    ]
  }, */

  { path: '*', redirect: '/404', hidden: true }
];

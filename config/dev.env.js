module.exports = {
  NODE_ENV: '"development"',
  ENV_CONFIG: '"dev"',
  BASE_API: '"https://s-report.ritx.xyz"',
  VUE_APP_BASE_API: '"https://api.ritx.xyz"',
};

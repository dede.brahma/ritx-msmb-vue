module.exports = {
  NODE_ENV: '"production"',
  ENV_CONFIG: '"prod"',
  BASE_API: '"https://ritxdashboard.com"',
  VUE_APP_BASE_API: '"https://api.ritx.xyz"'
};

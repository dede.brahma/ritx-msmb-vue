module.exports = {
  NODE_ENV: '"development"',
  ENV_CONFIG: '"sit"',
  BASE_API: '"https://s-report.ritx.xyz"',
  VUE_APP_BASE_API: '"https://s-api.ritx.xyz"'
};

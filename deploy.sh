#!/bin/bash

ENV=$1

echo "Running on $ENV environment"

if [ $ENV == "staging" ]; then
    BRANCH=docker
elif [ $ENV == "production" ]; then
    BRANCH=master
else
    echo "ENVIRONMENT not supported. Use staging or production."
    exit 1
fi


cd ~/report-dashboard

set -e

sudo ENVIRONMENT=$ENV make
sudo ENVIRONMENT=$ENV make stop
sudo ENVIRONMENT=$ENV make run

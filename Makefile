SERVICES = report-dashboard
ENVIRONMENT ?= staging

define make_docker
	docker build --tag report-dashboard/$(1):$(ENVIRONMENT) --build-arg BUILD_ENV=$(ENVIRONMENT)  --file Dockerfile .
endef

all: $(SERVICES)

.PHONY: all $(SERVICES)

$(SERVICES):
	$(call make_docker,$(@))

cleandocker: stop
	# Remove exited containers
	docker ps -f name=report-dashboard -f status=dead -f status=exited -aq | xargs -I F docker rm -v "F"

	# Remove unused images
	docker images "report-dashboard\/*" -f dangling=true -q | xargs -I F docker rmi "F"

	# Remove old ritx images
	docker images -q report-dashboard\/* | xargs -I F docker rmi --force "F"

run:
	ENVIRONMENT=$(ENVIRONMENT) docker-compose -f docker-compose.yml up -d
stop:
	# stop all service
	ENVIRONMENT=$(ENVIRONMENT) docker-compose -f docker-compose.yml stop
	
	# remove container
	docker ps -f name=report-dashboard -aq | xargs -I F docker rm "F"
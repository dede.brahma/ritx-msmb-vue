FROM node:8.16 as builder

ARG BUILD_ENV

ADD . /src
WORKDIR /src

WORKDIR /src

RUN yarn install && yarn build:$BUILD_ENV

FROM nginx:latest
COPY config/nginx.conf /etc/nginx/nginx.conf
COPY --from=builder /src/dist /usr/share/nginx/html/.
